import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.village.Village;
import net.minecraft.village.VillageCollection;
import net.villagevisualiser.villagevisualiser.VillageVisualiser;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.*;

import java.nio.FloatBuffer;

public class OtherRender {
	private int vao;
	private int vbo;
	private int size = 0;
	private ShaderProgram shader;

	public OtherRender(ShaderProgram shader){
		vao = GL30.glGenVertexArrays();
		vbo = GL15.glGenBuffers();

		// シェーダの共有
		this.shader = shader;

		init();
	}

	public void init(){
		// 定数
		final int FlaotSize = 4;

		// VAOバインド
		GL30.glBindVertexArray(vao);

		// VBOバインド
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo);

		// 頂点
		GL20.glEnableVertexAttribArray(0);
		GL20.glVertexAttribPointer(0,3,GL11.GL_FLOAT,false,7*FlaotSize,0);

		// 色
		GL20.glEnableVertexAttribArray(2);
		GL20.glVertexAttribPointer(2,4,GL11.GL_FLOAT,false,7*FlaotSize,3*FlaotSize);

		// VBOバインド解除
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);

		// VAOバインド解除
		GL30.glBindVertexArray(0);
	}

	public void setVillage(VillageCollection vc){
		size = vc.getVillageList().size();

		float buf[] = new float[size * 7];

		int i = 0;
		for(Village v : vc.getVillageList()) {
			buf[i*7]   = v.getCenter().getX(); // X
			buf[i*7+1] = v.getCenter().getY(); // Y
			buf[i*7+2] = v.getCenter().getZ(); // Z
			buf[i*7+3] = ColorManager.getR(i); // R
			buf[i*7+4] = ColorManager.getG(i); // G
			buf[i*7+5] = ColorManager.getB(i); // B
			buf[i*7+6] = 0.5f; // A
			i++;
		}

		FloatBuffer posBuffer = BufferUtils.createFloatBuffer(buf.length);
		posBuffer.put(buf).flip();
		GL30.glBindVertexArray(vao);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo);
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, posBuffer, GL15.GL_STREAM_DRAW);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
		GL30.glBindVertexArray(0);
	}

	public void draw() {
		// VAOのバインド
		GL30.glBindVertexArray(vao);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo);

		// なんかずれるので修正
		GlStateManager.pushMatrix();
		GlStateManager.translate(-1f,0.01f,0f);

		// シェーダを使用する
		shader.useProgram();
		shader.setScale(1);

		// Do something
		if(VillageVisualiser.ModConfig.doorSetting.DrawCenterPoint) {
			shader.setRound(true);
			GL11.glPointSize(10f);
			GL11.glDrawArrays(GL11.GL_POINTS, 0, size * 7);
			shader.setRound(false);
		}

		// シェーダプログラムをデフォルトに
		GL20.glUseProgram(0);

		GlStateManager.popMatrix();

		// VAOのバインドを解除
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
		GL30.glBindVertexArray(0);
	}

}
