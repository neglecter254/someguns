package someguns;

public class TimerCapablity implements ITimerCapability{

	private long initialTickCount;
	private long tickCount;
	private boolean isEnable;

	public TimerCapablity() {
		this.initialTickCount = 0;
		this.tickCount = 0;
		this.isEnable = false;
	}

	@Override
	public long getTick() {
		return this.tickCount;
	}

	@Override
	public double getProgress() {
		return (double)this.tickCount / this.initialTickCount;
	}

	@Override
	public boolean isFinish() {
		return this.isEnable && this.tickCount == 0;
	}

	@Override
	public boolean isRunning() {
		return this.isEnable;
	}

	@Override
	public void setTick(long tick) {
		this.initialTickCount = tick;
		this.tickCount = tick;
		this.isEnable = true;
	}

	@Override
	public void stop() {
		isEnable = false;
	}

	@Override
	public void addTick() {
		if(this.tickCount > 0)
			--this.tickCount;
	}
}
