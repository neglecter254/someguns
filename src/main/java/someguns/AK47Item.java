package someguns;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import someguns.BulletItem.BulletType;

public class AK47Item extends GunItem {

	public AK47Item(Properties builder) {
		super(builder);
		rate = 600; //rpm
		recoil = 5f;
		reloadtime = 20;
		bulletType = BulletType._762;
		velocity = 100f;
		damage = 5f;
		penetrate = false;
	}




}
//Entity bullet = new SnowballEntity(EntityType.SNOWBALL,world);
//bullet.setPosition(player.posX+v.x+bulletpos.x,player.posY+1.5f+v.y+bulletpos.y ,player.posZ+v.z+bulletpos.z);
//world.addEntity(bullet);