package someguns;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import org.apache.logging.log4j.LogManager;
import someguns.BulletItem.BulletType;

public class RailgunItem extends GunItem {

	public RailgunItem(Properties builder) {
		super(builder);
		bulletType = BulletType._762;
		rate = 30;         // rpm
		rate = 1200;         // rpm
		recoil = 5f;        // °
		reloadtime = 20;    // tick
		velocity = 1000f;   // m/sec
		rangeDistance = 100f;   // m
		damage = 20f;       // heart
		penetrate = true;   // 貫通
	}

	@Override
	void shot(ItemStack stack, World world, PlayerEntity player, Vec3d v){
		if(world.isRemote){ // Client
		}else{  // Server
			// 弾丸はレーザー
			LaserEntity bullet = new LaserEntity(
					world,
					player,
					player.getEyePosition(1.0f).add(v.scale(0.3d)),
					v.scale(velocity),
					this.rangeDistance,
					this.damage,
					this.penetrate);
			world.addEntity(bullet);

			if( !player.isCreative())
				decMagazine(stack);
		}

	}


}
//Entity bullet = new SnowballEntity(EntityType.SNOWBALL,world);
//bullet.setPosition(player.posX+v.x+bulletpos.x,player.posY+1.5f+v.y+bulletpos.y ,player.posZ+v.z+bulletpos.z);
//world.addEntity(bullet);