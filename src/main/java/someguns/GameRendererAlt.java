package someguns;

import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.resources.IResourceManager;
import net.minecraft.util.math.MathHelper;

public class GameRendererAlt extends GameRenderer {

	public GameRendererAlt(Minecraft mcIn, IResourceManager resourceManagerIn) {
		super(mcIn, resourceManagerIn);
	}

	private void applyBobbing(float partialTicks) {
		Minecraft mc = Minecraft.getInstance();
		if (mc.getRenderViewEntity() instanceof PlayerEntity) {
			PlayerEntity playerentity = (PlayerEntity)mc.getRenderViewEntity();
			float f = playerentity.distanceWalkedModified - playerentity.prevDistanceWalkedModified;
			float f1 = -(playerentity.distanceWalkedModified + f * partialTicks);
			float f2 = 0.1f;
			GlStateManager.translatef(MathHelper.sin(f1 * (float)Math.PI) * f2 * 0.5F, -Math.abs(MathHelper.cos(f1 * (float)Math.PI) * f2), 0.0F);
			GlStateManager.rotatef(MathHelper.sin(f1 * (float)Math.PI) * f2 * 3.0F, 0.0F, 0.0F, 1.0F);
			GlStateManager.rotatef(Math.abs(MathHelper.cos(f1 * (float)Math.PI - 0.2F) * f2) * 5.0F, 1.0F, 0.0F, 0.0F);
		}
	}

}
