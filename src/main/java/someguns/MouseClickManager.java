package someguns;

public class MouseClickManager {
	private static boolean right = false;
	private static boolean left = false;

	public static void setRight(boolean bool){
		right = bool;
	}
	public static boolean getRight(){
		return right;
	}
	public static void setLeft(boolean bool){
		left = bool;
	}
	public static boolean getLeft(){
		return left;
	}
}
