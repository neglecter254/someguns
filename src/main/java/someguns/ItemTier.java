package someguns;
import net.minecraft.item.IItemTier;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.Ingredient;

public enum ItemTier implements IItemTier{
	// Enumの宣言
	// マテリアルを設定
	MATERIAL_IRON(
			3,      /* 収穫能力*/
			1000,   /* 耐久度*/
			15.0f,  /* 効率*/
			3.0f,   /* 攻撃力*/
			25,     /* エンチャント能力*/
			Items.IRON_INGOT  /* 修理アイテム*/
	);

	// メンバ変数
	private float attackDamage, efficiency;
	private int maxUses, harvestLevel, enchantability;
	private Item repairMaterial;

	// コンストラクタ
	private ItemTier(int harvestLevelIn, int maxUsesIn, float efficiencyIn, float attackDamageIn, int enchantabilityIn, Item repairMaterialIn) {
		this.harvestLevel = harvestLevelIn;
		this.maxUses = maxUsesIn;
		this.efficiency = efficiencyIn;
		this.attackDamage = attackDamageIn;
		this.enchantability = enchantabilityIn;
		this.repairMaterial = repairMaterialIn;
	}

	// メソッド
	@Override
	public float getAttackDamage() {
		return attackDamage;
	}

	@Override
	public float getEfficiency() {
		return efficiency;
	}

	@Override
	public int getMaxUses() {
		return maxUses;
	}

	@Override
	public int getHarvestLevel() {
		return harvestLevel;
	}

	@Override
	public int getEnchantability() {
		return enchantability;
	}

	@Override
	public Ingredient getRepairMaterial() {
		return Ingredient.fromItems(this.repairMaterial);
	}
}