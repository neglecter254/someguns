package someguns;


import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.Minecraft;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.*;

import java.nio.FloatBuffer;


public class Render {
	static private Render render;
    static private ShaderProgram shader;
	static private int vao;
	static private int vbo;


	public Render(){
		getShader();

		vao = GL30.glGenVertexArrays();
		vbo = GL15.glGenBuffers();

		// floatのメモリサイズ
		final int FlaotSize = 4;

		// VAOバインド
		GL30.glBindVertexArray(vao);

		// VBOバインド
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo);

		// 頂点
		GL20.glVertexAttribPointer(0,3,GL11.GL_FLOAT,false,5*FlaotSize,0);

		// テクスチャ座標
		GL20.glVertexAttribPointer(1,2,GL11.GL_FLOAT,false,5*FlaotSize,3*FlaotSize);

		// 有効化
		GL20.glEnableVertexAttribArray(0);
		GL20.glEnableVertexAttribArray(1);

		// VBOバインド解除
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);

		// VAOバインド解除
		GL30.glBindVertexArray(0);
    }

	static public Render getRender(){
		// シェーダの作成
		if(render == null) {
			render = new Render();
		}
		return render;
	}
    static public ShaderProgram getShader(){
	    // シェーダの作成
	    if(shader == null) {
			shader = new ShaderProgram();
		}
		return shader;
    }

    static public void AdaptVAO(){
	    // VAOのバインド
	    GL30.glBindVertexArray(vao);
	    GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo);
    }
	static public void DefaultVAO(){
		// VAOのアンバインド
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
		GL30.glBindVertexArray(0);
	}

	static public void setInterLeaveArray(float[] buf){
		//　インターリーブ配列をGPUに送信
		AdaptVAO();
		FloatBuffer vertexBuffer = BufferUtils.createFloatBuffer(buf.length);
		vertexBuffer.put(buf).flip();
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, vertexBuffer, GL15.GL_STREAM_DRAW);
		//GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
		//GL30.glBindVertexArray(0);

	}

/*
	public void drawInstancing(RenderWorldLastEvent event){
        EntityPlayer p = Minecraft.getMinecraft().player;
        double diffX = p.lastTickPosX+(p.posX-p.lastTickPosX)*event.getPartialTicks();
        double diffY = p.lastTickPosY+(p.posY-p.lastTickPosY)*event.getPartialTicks();
        double diffZ = p.lastTickPosZ+(p.posZ-p.lastTickPosZ)*event.getPartialTicks();

        GlStateManager.pushMatrix();
        GlStateManager.pushAttrib();
        GlStateManager.disableLighting();
        GlStateManager.enableAlpha();
        GlStateManager.enableBlend();
        GlStateManager.blendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GlStateManager.translate(-(diffX -0.5d) , -(diffY +0d) ,-(diffZ -0.5d));
		GL11.glEnable(GL20.GL_POINT_SPRITE);
	    GlStateManager.disableCull();

	    // ドーム表示
	    domeRender.draw();

	    // ドアのライン表示
	    doorsRender.draw();

	    // その他の表示
		otherRender.draw();

*/
        // debug表示
	    /*
	    FontRenderer fontRenderer = Minecraft.getMinecraft().getRenderManager().getFontRenderer();
	    float[] v = dome.getVertices();
	    if(fontRenderer != null) {
		    for(int i = 0; i < v.length/3; i++) {
		        GlStateManager.pushMatrix();
			    GL11.glScaled(32F,32F,32F);
			    GL11.glTranslated(v[i*3],v[i*3+1],v[i*3+2]);
			    GL11.glScaled(1/32F,1/32F,1/32F);
			    GL11.glTranslated(0,36,0);
			    GL11.glRotatef(180F,1.0F,0.0F,0.0F);
			    GL11.glScaled(1/10F,1/10F,1/10F);
			    String text = Integer.toString(i);
			    fontRenderer.drawString(text, -fontRenderer.getStringWidth(text) / 2, 0, 0x00FF00);
		        GlStateManager.popMatrix();
		    }
	    }*/

	    ///GlStateManager.enableCull();
	    //GlStateManager.disableBlend();
        //GlStateManager.popMatrix();
        //GlStateManager.popAttrib();
    //}


}
