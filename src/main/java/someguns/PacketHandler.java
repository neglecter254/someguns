package someguns;


import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkRegistry;
import net.minecraftforge.fml.network.simple.SimpleChannel;

public class PacketHandler {
	private static final String PROTOCOL_VERSION = "1";
	public static final SimpleChannel INSTANCE = NetworkRegistry.newSimpleChannel(
			new ResourceLocation(SomeGuns.MODID, "main"),
			() -> PROTOCOL_VERSION,
			PROTOCOL_VERSION::equals,
			PROTOCOL_VERSION::equals
	);

	public static void register(){
		/*IMesssageHandlerクラスとMessageクラスの登録。
		 * registerMessage
		 * 第一引数：MessageクラスのMOD内での登録ID。
		 * 第二引数：MessageクラスIMessageの実装
		 * 第三引数：encoder msgからbuffへ変換 Imessage<T>,packetbuffer -> void
		 * 第四引数:decoder buffからmsgへ変換 packetbuffer -> Imessage<T>
		 * 第五引数：handler パケットに対する実際の処理　msg,context -> void*/
		int id = 0;

		INSTANCE.registerMessage(id++, ShotMessage.class, ShotMessage::encode, ShotMessage::decode, ShotMessage::handle);
		INSTANCE.registerMessage(id++, ReloadMessage.class, ReloadMessage::encode, ReloadMessage::decode, ReloadMessage::handle);
		INSTANCE.registerMessage(id++, HitMessage.class, HitMessage::encode, HitMessage::decode, HitMessage::handle);


	}


}
