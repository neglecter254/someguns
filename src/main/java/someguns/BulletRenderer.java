package someguns;

import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.entity.player.AbstractClientPlayerEntity;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.model.BoatModel;
import net.minecraft.entity.item.BoatEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.opengl.GL11;
import org.lwjgl.system.windows.POINT;

import javax.annotation.Nullable;


@OnlyIn(Dist.CLIENT)
public class BulletRenderer extends EntityRenderer<BulletEntity>{
	protected static final Logger LOGGER = LogManager.getLogger();
	private static final ResourceLocation TEXTURES = new ResourceLocation("textures/block/gold_block.png");
	protected final BulletMedel bulletmodel = new BulletMedel();

	protected BulletRenderer(EntityRendererManager renderManager) {
		super(renderManager);
		this.shadowSize = 0.1F;
	}

	public void doRender(BulletEntity entity, double x, double y, double z, float entityYaw, float partialTicks) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(x,y,z);
		GlStateManager.rotatef(-entityYaw+90, 0.0F, 1.0F, 0.0F);
		GlStateManager.rotatef(entity.rotationPitch, 0.0F, 0.0F, 1.0F);

		//renderOffsetAABB(new AxisAlignedBB(0,0,0,1,1,1), 0,0,0);

		//GL11.glPointSize(100);
		//GlStateManager.begin(GL11.GL_LINES);
		//GlStateManager.vertex3f(0.0f , 0.0f,0.0f);
		//GlStateManager.vertex3f( 200f, 0.0f,0.0f);
		//GlStateManager.end();

		bindEntityTexture(entity);
		bulletmodel.render( 0.01F);

		GlStateManager.popMatrix();

		super.doRender(entity, x, y, z, entityYaw, partialTicks);

	}


	@Nullable
	@Override
	protected ResourceLocation getEntityTexture(BulletEntity entity) {
		return TEXTURES;
	}
}

