package someguns;


import io.netty.buffer.ByteBuf;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.function.Supplier;

public class ShotMessage {  //Imessageは廃止
	// Vec3dを渡すのが無いのでwriteBlockRayを使う
	Vec3d start;
	Vec3d vector;

	public ShotMessage(Vec3d start_, Vec3d vector_) {
		this.start = start_;
		this.vector = vector_;
	}

	public static void encode(ShotMessage msg, PacketBuffer buf) {
		buf.writeFloat((float)msg.start.getX());
		buf.writeFloat((float)msg.start.getY());
		buf.writeFloat((float)msg.start.getZ());
		buf.writeFloat((float)msg.vector.getX());
		buf.writeFloat((float)msg.vector.getY());
		buf.writeFloat((float)msg.vector.getZ());
	}

	public static ShotMessage decode(PacketBuffer buf) {
		double x1 = buf.readFloat();
		double y1 = buf.readFloat();
		double z1 = buf.readFloat();
		double x2 = buf.readFloat();
		double y2 = buf.readFloat();
		double z2 = buf.readFloat();
		return new ShotMessage(new Vec3d(x1,y1,z1),new Vec3d(x2,y2,z2));
	}

	public static void handle(final ShotMessage message, Supplier<NetworkEvent.Context> ctx) {
		ctx.get().enqueueWork(() -> {

			Item item = ctx.get().getSender().getActiveItemStack().getItem();
			if(item instanceof GunItem) {
				((GunItem) item).shot(
						ctx.get().getSender().getHeldItemMainhand(),
						ctx.get().getSender().getServerWorld(),
						ctx.get().getSender(),
						message.vector
						);
			}
		});
		ctx.get().setPacketHandled(true);
	}

}
