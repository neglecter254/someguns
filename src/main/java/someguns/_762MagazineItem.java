package someguns;


import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import someguns.BulletItem.BulletType;


import javax.annotation.Nullable;
import java.util.List;

public class _762MagazineItem extends MagazineItem {

	public _762MagazineItem(Properties builder) {
		//super(tier, attackDamageIn, attackSpeedIn, builder);
		super(builder);
		bulletType = BulletType._762;
		capacity = 30;
	}



	@Override
	public int getItemStackLimit(ItemStack stack){
		// スタックさせない
		return 1;
	}



}
//Entity bullet = new SnowballEntity(EntityType.SNOWBALL,world);
//bullet.setPosition(player.posX+v.x+bulletpos.x,player.posY+1.5f+v.y+bulletpos.y ,player.posZ+v.z+bulletpos.z);
//world.addEntity(bullet);