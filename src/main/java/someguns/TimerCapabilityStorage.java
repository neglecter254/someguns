package someguns;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;

import javax.annotation.Nullable;

public class TimerCapabilityStorage implements Capability.IStorage<ITimerCapability> {

	@Nullable
	@Override
	public INBT writeNBT(Capability<ITimerCapability> capability, ITimerCapability instance, Direction side) {
		CompoundNBT tag = new CompoundNBT();
		tag.putLong("tick", instance.getTick());
		return tag;
	}

	@Override
	public void readNBT(Capability<ITimerCapability> capability, ITimerCapability instance, Direction side, INBT nbt) {
		instance.setTick(((CompoundNBT)nbt).getLong("tick"));
	}
}
