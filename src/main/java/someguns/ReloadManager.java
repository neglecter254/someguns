package someguns;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.SoundCategory;
import net.minecraftforge.event.TickEvent;

import java.util.concurrent.atomic.AtomicBoolean;

public class ReloadManager {
	static ItemStack lastSelectItem;

	public static void sendServer(){
		if(Minecraft.getInstance().getConnection() != null)
			PacketHandler.INSTANCE.sendToServer(new ReloadMessage());
	}
	public static void tick(TickEvent.PlayerTickEvent event) {
		// 毎tick リロードタイマー加算
		if (event.side.isClient()) {
			event.player.getCapability(TimerCapabilityProvider.TIMER_CAPABILITY).ifPresent((t) -> {
				if(lastSelectItem == event.player.getHeldItemMainhand()) {
					t.addTick();
					if (t.isFinish()) {
						t.stop();
						event.player.world.playSound(event.player, event.player.posX, event.player.posY, event.player.posZ, SomeGuns.RELOAD, SoundCategory.AMBIENT, 1f, 1.0f);

						// サーバでリロード実行
						ReloadManager.sendServer();
					}
				}else {
					t.stop();
				}
			});
		}
	}
	public static void startReload(){
		PlayerEntity player = Minecraft.getInstance().player;
		ItemStack stack = player.getHeldItemMainhand();
		if(stack.getItem() instanceof GunItem) {
			player.getCapability(TimerCapabilityProvider.TIMER_CAPABILITY).ifPresent((t) -> {
				// すでにReload中ではないこと マガジンがあること
				if(!t.isRunning() && ((GunItem)(stack.getItem())).findAmmo(player).getKey() != ItemStack.EMPTY) {
					// 現在の選択itemstackを記憶しておく
					lastSelectItem = player.getHeldItemMainhand();
					// ぐるぐる表示
					OverlayRender.setReloadStartTime(((GunItem) stack.getItem()).reloadtime);
					// timerセット
					t.setTick(((GunItem) stack.getItem()).reloadtime);
					// 音
					player.world.playSound(player,player.posX, player.posY, player.posZ, SomeGuns.RELOAD, SoundCategory.AMBIENT, 1f, 2.0f);
				}
			});
		}
	}
	public static boolean isReloading() {
		PlayerEntity player = Minecraft.getInstance().player;
		AtomicBoolean res = new AtomicBoolean(false);
		player.getCapability(TimerCapabilityProvider.TIMER_CAPABILITY).ifPresent((t) -> {
			res.set(t.isRunning());
		});
		return res.get();
	}
}
