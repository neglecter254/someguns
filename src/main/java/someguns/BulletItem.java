package someguns;

import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.IItemTier;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.UseAction;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.security.PublicKey;


public class BulletItem extends Item {

	public BulletType bulletType;

	public BulletItem(Properties builder) {
		//super(tier, attackDamageIn, attackSpeedIn, builder);
		super(builder);
	}

	public void inventoryTick(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected) {
		// 毎チック呼ばれる
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World world, PlayerEntity player, Hand hand) {
		// クリックホールドすると4tickに一回しかこのメソッドが呼ばれないので連射に向かない
		return ActionResult.newResult(ActionResultType.FAIL, player.getHeldItem(hand));
	}

	@Override
	public UseAction getUseAction(ItemStack stack) {
		return UseAction.NONE;
	}

	@Override
	public void onPlayerStoppedUsing(ItemStack stack, World worldIn, LivingEntity entityLiving, int timeLeft) { }

	public enum BulletType{
		_762,
		_556
	}

}
//Entity bullet = new SnowballEntity(EntityType.SNOWBALL,world);
//bullet.setPosition(player.posX+v.x+bulletpos.x,player.posY+1.5f+v.y+bulletpos.y ,player.posZ+v.z+bulletpos.z);
//world.addEntity(bullet);