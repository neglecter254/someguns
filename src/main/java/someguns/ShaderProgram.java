package someguns;

import com.mojang.blaze3d.platform.GlStateManager;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.logging.log4j.LogManager;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.FloatBuffer;
import java.util.logging.Logger;

public class ShaderProgram {
	/*
	まずバーテックスシェーダおよびフラグメントシェーダ, そして必要ならジオメトリシェーダのシェーダオブジェクトを作成します (glCreateShader()).
	作成したそれぞれのシェーダオブジェクトに対して, ソースプログラムを読み込みます (glShaderSource()).
	読み込んだソースプログラムをコンパイルします (glCompileShader()).
	プログラムオブジェクトを作成します (glCreateProgram()).
	プログラムオブジェクトに対してシェーダオブジェクトを登録します (glAttachShader()).
	プログラムオブジェクトをリンクします (glLinkProgram()).
	プログラムオブジェクトを適用します (glUseProgram()).
	 */
	private int programId;
	private FloatBuffer ModelView;
	private FloatBuffer Projection;
	private float Time;
	private int ModelViewLoc;
	private int ProjectionLoc;
	private int TimeLoc;
	private int SizeLoc;
	private int RoundFlagLoc;
	private String vertexShaderFilename = "vert";
	private String fragmentShaderFilename = "frag";

	public ShaderProgram()
	{
		// プログラムオブジェクトを作成
		programId = GL20.glCreateProgram();

		// シェーダオブジェクトを作成
		int vertShader = GL20.glCreateShader(GL20.GL_VERTEX_SHADER);
		int fragShader = GL20.glCreateShader(GL20.GL_FRAGMENT_SHADER);

		// ソースプログラムを読み込み
		GL20.glShaderSource(vertShader, loadFile(vertexShaderFilename));
		GL20.glShaderSource(fragShader, loadFile(fragmentShaderFilename));

		// コンパイル
		compileShader(vertShader);
		compileShader(fragShader);

		// プログラムオブジェクトにシェーダオブジェクトを登録
		GL20.glAttachShader(programId, vertShader);
		GL20.glAttachShader(programId, fragShader);

		// プログラムオブジェクトをリンク
		GL20.glLinkProgram(programId);

		// perform general validation that the program is usable
		GL20.glValidateProgram(programId);

		// uniform変数用バッファ
		ModelView = BufferUtils.createFloatBuffer(16);
		Projection = BufferUtils.createFloatBuffer(16);

		// uniform変数のロケーション
		ModelViewLoc = GL20.glGetUniformLocation(this.programId, "ModelView");
		ProjectionLoc = GL20.glGetUniformLocation(this.programId, "Projection");
		TimeLoc = GL20.glGetUniformLocation(this.programId, "Time");

		LogManager.getLogger().info("ModlViewLoc="+ModelViewLoc);
		LogManager.getLogger().info("ProjectionLoc="+ProjectionLoc);
		LogManager.getLogger().info("Time="+TimeLoc);

	}

	public void useProgram(){

		GL20.glUseProgram(programId);
		// WVP変換行列をシェーダにセット
		setWorldViewProjection();
	}

	public void setPartialTime(float t){
		GL20.glUniform1f(TimeLoc, t);
	}

	public void setScale(float size){
		GL20.glUniform1f(SizeLoc, size);
	}

	public void setRound(boolean flag){
		GL20.glUniform1f(RoundFlagLoc, (float)BooleanUtils.toInteger(flag));
	}

	private void setWorldViewProjection() {
		GlStateManager.getMatrix(GL11.GL_MODELVIEW_MATRIX,ModelView);
		GlStateManager.getMatrix(GL11.GL_PROJECTION_MATRIX,Projection);
		GL20.glUniformMatrix4fv(ModelViewLoc, false, ModelView);
		GL20.glUniformMatrix4fv(ProjectionLoc, false, Projection);

	}

	/**
	 * Load a text file and return it as a String.
	 */
	private String loadFile(String filename)
	{
		StringBuilder vertexCode = new StringBuilder();
		String line = null ;
		try {
			URL url = getClass().getClassLoader().getResource(filename);
			BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
			while ((line = reader.readLine()) != null) {
				vertexCode.append(line);
				vertexCode.append('\n');
			}
		}catch(Exception e){
			throw new IllegalArgumentException("unable to load resource from file ["+filename+"]", e);
		}

		return vertexCode.toString();
	}

	private void compileShader(int shaderID ){
		GL20.glCompileShader(shaderID);
		if(GL20.glGetShaderi(shaderID, GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE) {
			System.err.println("Shader failed to compile! [" + shaderID + "]");
			System.err.println(GL20.glGetShaderInfoLog(shaderID, 1024));
		}
	}
	public int getProgramId() {
		return programId;
	}
}


