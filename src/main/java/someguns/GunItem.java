package someguns;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.client.particle.Particle;
import net.minecraft.client.particle.SmokeParticle;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.command.arguments.EntityAnchorArgument;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.*;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.play.server.SPlayerLookPacket;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.items.ItemStackHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import someguns.BulletItem.BulletType;

import javax.annotation.Nullable;
import java.util.List;

//弾の永続化まだ
public class GunItem extends Item {
	private static final Logger LOGGER = LogManager.getLogger();
	public BulletType bulletType;
	protected int rate;         // rpm
	protected float recoil;     // 謎係数
	protected int reloadtime;   // tick
	protected float velocity;   // m/sec
	protected float rangeDistance = 100f;  // m
	protected float damage;     // heart
	protected boolean penetrate;    // 貫通するか

	private long cPrevShotTime = 0;
	private double inaccurate = 0;

	public GunItem( Properties builder) {
		//super(tier, attackDamageIn, attackSpeedIn, builder);
		super(builder);
	}

	public void inventoryTick(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected) {
		// 毎チック呼ばれる
		if (!(entityIn instanceof PlayerEntity)) return;
		PlayerEntity player = (PlayerEntity) entityIn;

		// 発射判定はクライアントのみで行い、発射したらパケットでサーバに伝える
		if (worldIn.isRemote){
			//LOGGER.info(worldIn.getGameTime());
			//LOGGER.info(player.getHeldItemMainhand() == stack);
			//LOGGER.info(player.isHandActive());
			//LOGGER.info(player.getHeldItemMainhand().getDisplayName());
			//LOGGER.info(player.getActiveItemStack().getDisplayName());
			// アイテム使用中に移動速度低下するのを打ち消す

			if(player.getHeldItemMainhand() == stack && MouseClickManager.getRight() && !ReloadManager.isReloading()) {
				if (player.isHandActive()) {
					((ClientPlayerEntity)player).movementInput.moveStrafe *= 5;
					((ClientPlayerEntity)player).movementInput.moveForward *= 5;
				}

				if (worldIn.getGameTime() < cPrevShotTime)
					cPrevShotTime = 0;

				boolean flag = false;

				if (worldIn.getGameTime() - cPrevShotTime >= (20d / (rate / 60d))) {
					if (getMagazineAmmo(stack) > 0 || player.isCreative() ) {
						cPrevShotTime = worldIn.getGameTime();
						flag = true;
					}
				}
				if (flag) {

					Vec3d v = getVectorForRotation(player.rotationPitch, player.rotationYawHead);
					v = diffusion(worldIn,v,inaccurate);

					shot(stack, worldIn, player, v);
					// サーバに送信
					//PacketHandler.INSTANCE.sendToServer(new ShotMessage(player.getPositionVec(), v));
					PacketHandler.INSTANCE.sendToServer(new ShotMessage(new Vec3d(player.prevPosX,player.prevPosY,player.prevPosZ), v));

					// 銃声
					worldIn.playSound(player, player.posX, player.posY, player.posZ, SomeGuns.FIRING, SoundCategory.AMBIENT, 0.5f, 1.0f);
					// リコイル
					player.rotationPitch -= recoil;
					// 画面を揺らす
					player.hurtTime = 2;
					player.maxHurtTime = 4;

					// 精度悪化
					this.inaccurate = 0.08d;

				}
			}else if(!MouseClickManager.getRight()){
				// 精度リセット
				this.inaccurate = 0d;
			}
		}
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World world, PlayerEntity player, Hand hand) {
		// クリックホールドすると4tickに一回しかこのメソッドが呼ばれないので連射に向かない
		player.setActiveHand(hand);
		if(!(player.isCreative() && !player.onGround))
			player.setSprinting(false);
		return ActionResult.newResult(ActionResultType.FAIL, player.getHeldItem(hand));
	}

	@Override
	public UseAction getUseAction(ItemStack stack) {
		return UseAction.NONE;
	}

	@Override
	public void onPlayerStoppedUsing(ItemStack stack, World worldIn, LivingEntity entityLiving, int timeLeft) {

	}
	@Override
	public int getUseDuration(ItemStack stack) {
		return 72000;
	}
	@Override
	public int getItemStackLimit(ItemStack stack){
		return 1;
	}

	public Vec3d diffusion(World world,Vec3d v,double inaccurate){
		// 既存のベクトルをベクトルに垂直な回転軸回りにamountだけ回転させる
		if(inaccurate == 0)
			return v;

		// 1rad ≑ 60°
		double amount = world.getRandom().nextGaussian() * inaccurate;

		// ランダムなベクトル
		Vec3d vr = new Vec3d(world.getRandom().nextDouble(),world.getRandom().nextDouble(),world.getRandom().nextDouble());

		// 元のベクトルと外積をとって垂直なベクトルにする
		vr = vr.crossProduct(v).normalize();

		// ロドリゲスの回転公式 v′ = v cosθ + n ( v ⋅ n ) ( 1−cosθ ) + ( n × v )  sinθ
		vr = v.scale(Math.cos(amount)).add(vr.scale(v.dotProduct(vr)).scale(1-Math.cos(amount))).add(vr.crossProduct(v).scale(Math.sin(amount)));

		return vr;
	}

	void shot(ItemStack stack, World world, PlayerEntity player,Vec3d v){


		if(world.isRemote){ // Client
		}else{  // Server
			// 弾丸
			BulletEntity bullet = new BulletEntity(
					world,
					player,
					player.getEyePosition(1.0f).add(v.scale(0.3d)),
					v.scale(velocity),
					this.rangeDistance,
					this.damage,
					this.penetrate);
			world.addEntity(bullet);

			if( !player.isCreative())
				decMagazine(stack);

		}
		//player.resetCooldown();
		//player.getCooldownTracker().removeCooldown(this);
	}

	public static Vec3d getVectorForRotation(float pitch, float yaw){
		float f = pitch * ((float)Math.PI / 180F);
		float f1 = -yaw * ((float)Math.PI / 180F);
		float f2 = MathHelper.cos(f1);
		float f3 = MathHelper.sin(f1);
		float f4 = MathHelper.cos(f);
		float f5 = MathHelper.sin(f);
		return new Vec3d((double)(f3 * f4), (double)(-f5), (double)(f2 * f4));
	}

	public javafx.util.Pair findAmmo(PlayerEntity player){
		// playerのインベントリから同じ弾のマガジンを見つける
		// 弾数が最も多いマガジンを優先する
		// 弾数は1発以上残っているもの
		ItemStack returnItem = ItemStack.EMPTY;
		int slot = 0;
		int ammoCount = 0;
		for(int i = 0; i < player.inventory.getSizeInventory(); i++) {
			ItemStack itemstack = player.inventory.getStackInSlot(i);
			if (itemstack.getItem() instanceof MagazineItem && ((MagazineItem)itemstack.getItem()).bulletType == this.bulletType) {
				if(MagazineItem.getAmmo(itemstack) > ammoCount) {
					returnItem = itemstack;
					slot = i;
					ammoCount = MagazineItem.getAmmo(itemstack);
				}
			}
		}
		return new javafx.util.Pair(returnItem,slot);
	}

	// server only
	public void reload(ItemStack stack,PlayerEntity player){

		// 新しいマガジンをプレイヤーインベントリから削除して銃に取り込み、古いマガジンをプレイヤーインベントリ内に作成する
		javafx.util.Pair pair = findAmmo(player);
		ItemStack newMagazine = (ItemStack) pair.getKey();
		int slot = (int)pair.getValue();
		if(newMagazine == ItemStack.EMPTY)
			return;

		CompoundNBT nbt = stack.getOrCreateTag();
		ItemStack magazine = ItemStack.read(nbt.getCompound("magazine"));

		// Itemstackに新しいマガジンを保存
		nbt.put("magazine",newMagazine.serializeNBT());
		// 古いマガジンをインベントリへ上書き
		player.inventory.setInventorySlotContents(slot,magazine);

	}

	@OnlyIn(Dist.CLIENT)
	public static void setGUIText(ItemStack stack){
		OverlayRender.setRemainText(String.format("%3s", getMagazineAmmo(stack)).replace(" ", "0"));
		OverlayRender.setCapacityText(String.format("%3s", getMagazineCapacity(stack)).replace(" ", "0"));
	}


	@Override
	@OnlyIn(Dist.CLIENT)
	public void addInformation(ItemStack stack, @Nullable World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
		tooltip.add(new StringTextComponent(TextFormatting.AQUA + ("Ammo: " + getMagazineAmmo(stack) + " / " +  getMagazineCapacity(stack))));
		super.addInformation(stack, worldIn, tooltip, flagIn);
	}

	protected static int getMagazineAmmo(ItemStack gunItemStack){
		ItemStack itemstack = getMagazine(gunItemStack);
		if(!itemstack.isEmpty()) {
			CompoundNBT nbt = itemstack.getTag();
			if (nbt != null) {
				return nbt.getInt("ammo");
			}
		}
		return 0;
	}

	protected static int getMagazineCapacity(ItemStack gunItemStack){
		ItemStack itemstack = getMagazine(gunItemStack);
		if(!itemstack.isEmpty()) {
			return ((MagazineItem)itemstack.getItem()).capacity;
		}
		return 0;
	}

	protected static ItemStack getMagazine(ItemStack gunItemStack){
		return ItemStack.read(gunItemStack.getOrCreateTag().getCompound("magazine"));
	}
	protected void setMagazine(ItemStack gunItemStack,ItemStack magazineItemStack){
		gunItemStack.getOrCreateTag().put("magazine",magazineItemStack.serializeNBT());
	}
	protected void decMagazine(ItemStack gunItemStack){
		ItemStack magazine = getMagazine(gunItemStack);
		((MagazineItem)magazine.getItem()).dec(magazine);
		setMagazine(gunItemStack,magazine);

	}
}
//Entity bullet = new SnowballEntity(EntityType.SNOWBALL,world);
//bullet.setPosition(player.posX+v.x+bulletpos.x,player.posY+1.5f+v.y+bulletpos.y ,player.posZ+v.z+bulletpos.z);
//world.addEntity(bullet);