package someguns;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import someguns.BulletItem.BulletType;

import javax.annotation.Nullable;
import java.util.List;

public class MagazineItem extends Item {
	// 空マガジン
	public BulletType bulletType;
	public int capacity;    // 装弾数

	public MagazineItem(Properties builder) {
		//super(tier, attackDamageIn, attackSpeedIn, builder);
		super(builder);
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World world, PlayerEntity player, Hand hand) {
		// 弾丸を取り込む
		ItemStack itemstack = player.getHeldItem(hand);
		ItemStack ammoStack = findAmmo(player);
		if (ammoStack.isEmpty())
			return ActionResult.newResult(ActionResultType.FAIL, player.getHeldItem(hand));

		CompoundNBT nbt = itemstack.getOrCreateTag();
		int ammo = nbt.getInt("ammo");

		if (ammo < capacity) {
			if (!world.isRemote) {
				++ammo;
				ammoStack.shrink(1);
				if (ammoStack.isEmpty())
					player.inventory.deleteStack(ammoStack);
				nbt.putInt("ammo",ammo);
			}
			return ActionResult.newResult(ActionResultType.SUCCESS, player.getHeldItem(hand));
		}
		return ActionResult.newResult(ActionResultType.FAIL, player.getHeldItem(hand));
	}

	private ItemStack findAmmo(PlayerEntity player){
		for(int i = 0; i < player.inventory.getSizeInventory(); ++i) {
			ItemStack itemstack = player.inventory.getStackInSlot(i);
			if (itemstack.getItem() instanceof BulletItem && ((BulletItem)itemstack.getItem()).bulletType == this.bulletType) {
				return itemstack;
			}
		}
		return ItemStack.EMPTY;
	}

	public void dec(ItemStack stack) {
		CompoundNBT nbt = stack.getOrCreateTag();
		if(nbt.getInt("ammo") > 0)
			nbt.putInt("ammo",nbt.getInt("ammo")-1);
	}

	public static int getAmmo(ItemStack stack) {
		return stack.getOrCreateTag().getInt("ammo");
	}

	@Override
	public void onCreated(ItemStack stack, World worldIn, PlayerEntity player) {
		// デフォルトで弾数1
		if(bulletType != null)
			stack.getOrCreateTag().putInt("ammo",1);
	}

	@Override
	@OnlyIn(Dist.CLIENT)
	public void addInformation(ItemStack stack, @Nullable World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
		CompoundNBT nbt = stack.getTag();
		if(nbt != null) {
			int ammo = nbt.getInt("ammo");
			if(ammo < capacity)
				tooltip.add(new StringTextComponent(TextFormatting.RED + ("Ammo: " + ammo + " / " + capacity)));
			else
				tooltip.add(new StringTextComponent(TextFormatting.AQUA + ("Ammo: " + ammo + " / " + capacity)));
		}
		super.addInformation(stack, worldIn, tooltip, flagIn);
	}

}
//Entity bullet = new SnowballEntity(EntityType.SNOWBALL,world);
//bullet.setPosition(player.posX+v.x+bulletpos.x,player.posY+1.5f+v.y+bulletpos.y ,player.posZ+v.z+bulletpos.z);
//world.addEntity(bullet);