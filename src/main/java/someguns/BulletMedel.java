package someguns;

import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.model.BoatModel;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.entity.model.ShulkerBulletModel;
import net.minecraft.entity.item.BoatEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import static net.minecraft.client.renderer.entity.EntityRenderer.renderOffsetAABB;

@OnlyIn(Dist.CLIENT)
public class BulletMedel extends EntityModel<BulletEntity> {

	private final RendererModel body;
	public BulletMedel() {
		body = (new RendererModel(this, 0, 0)).setTextureSize(14, 14);
		body.addBox(-8F, -4F, -4F, 16, 8, 8, 0.0f);
	}

	public void render(float scale) {
		body.render(scale);
	}

}
