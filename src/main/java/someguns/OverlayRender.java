package someguns;

import com.mojang.blaze3d.platform.GLX;
import com.mojang.blaze3d.platform.GlStateManager;
import javafx.util.Pair;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Matrix4f;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec2f;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import org.lwjgl.glfw.GLFWGamepadState;
import org.lwjgl.opengl.GL11;

import javax.vecmath.Vector2f;
import java.util.HashMap;

import static someguns.SomeGuns.MODID;

@OnlyIn(Dist.CLIENT)
public class OverlayRender {
	private static float hitmarkTransparency = 0.0f;
	private static final ResourceLocation TEXTURE_REMAIN = new ResourceLocation(MODID, "/textures/gui/number.png");
	private static String remainText = "";
	private static String capacityText = "";
	private static boolean enableText = false;
	private static double reloadStartTime;
	private static double reloadNeedTime;


	// テクスチャサイズ
	final static int SIZE = 256;

	// ハッシュマップ
	public static HashMap<Character , Vector2f> texCoord = new HashMap<Character, Vector2f>();
	public static void initTexCoord() {
		texCoord.put('0', new Vector2f(1 / (float)4 * 0, 1 / (float)3 * 0));
		texCoord.put('1', new Vector2f(1 / (float)4 * 1, 1 / (float)3 * 0));
		texCoord.put('2', new Vector2f(1 / (float)4 * 2, 1 / (float)3 * 0));
		texCoord.put('3', new Vector2f(1 / (float)4 * 3, 1 / (float)3 * 0));
		texCoord.put('4', new Vector2f(1 / (float)4 * 0, 1 / (float)3 * 1));
		texCoord.put('5', new Vector2f(1 / (float)4 * 1, 1 / (float)3 * 1));
		texCoord.put('6', new Vector2f(1 / (float)4 * 2, 1 / (float)3 * 1));
		texCoord.put('7', new Vector2f(1 / (float)4 * 3, 1 / (float)3 * 1));
		texCoord.put('8', new Vector2f(1 / (float)4 * 0, 1 / (float)3 * 2));
		texCoord.put('9', new Vector2f(1 / (float)4 * 1, 1 / (float)3 * 2));
		texCoord.put('/', new Vector2f(1 / (float)4 * 2, 1 / (float)3 * 2));

	}
	public static void setRemainText(String text){
		remainText = text;
	}
	public static void setCapacityText(String text){
		capacityText = text;
	}
	public static void setVisibleText(boolean f){
		enableText = f;
	}

	public static void resetHitmark(){
		hitmarkTransparency = 0.8f;
	}
	public static void hitmarkRender(float partialTick){
		int width = Minecraft.getInstance().mainWindow.getScaledWidth();
		int height = Minecraft.getInstance().mainWindow.getScaledHeight();


		GlStateManager.pushMatrix();
		GlStateManager.translated(width/2,height/2,1);
		GlStateManager.rotatef(45, 0.0F, 0.0F, 1.0F);
		GlStateManager.scalef(1.2f,1.2f,1.2f);
		GlStateManager.enableBlend();
		GlStateManager.disableAlphaTest();
		GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
		GL11.glEnable(GL11.GL_POLYGON_SMOOTH);
		//GlStateManager.alphaFunc(516, 0.5F);
		//GlStateManager.disableCull();
		//GlStateManager.disableLighting();



		for(int i = 0; i < 4; i++) {
			//　黒縁
			GL11.glColor4f(0.0f, 0.0F, 0.0F, hitmarkTransparency);
			GlStateManager.begin(GL11.GL_TRIANGLES);
			GlStateManager.vertex3f(4.0f, 0.0f, 0.0f);
			GlStateManager.vertex3f(10.2f, 1.2f, 0);
			GlStateManager.vertex3f(10.2f, -1.2f, 0);
			GlStateManager.end();

			GL11.glColor4f(1.0f, 1.0F, 1.0F, hitmarkTransparency);
			GlStateManager.begin(GL11.GL_TRIANGLES);
			GlStateManager.vertex3f(5.0f, 0.0f, 0.0f);
			GlStateManager.vertex3f(10f, 1f, 0);
			GlStateManager.vertex3f(10f, -1f, 0);
			GlStateManager.end();

			GlStateManager.rotatef(90, 0.0F, 0.0F, 1.0F);
		}

		GL11.glDisable(GL11.GL_POLYGON_SMOOTH);
		GlStateManager.popMatrix();

		//　1フレーム毎に薄く
		if(hitmarkTransparency>= 0.00001f)
			hitmarkTransparency -= 1/40f;
	}

	public static void HUDRender(float partialTick){

		if(!enableText)
			return;

		// 透視投影
		GlStateManager.matrixMode( GL11.GL_PROJECTION );
		GlStateManager.pushMatrix();
		GlStateManager.loadIdentity();
		GlStateManager.multMatrix(Matrix4f.perspective(80.0D, (float)Minecraft.getInstance().mainWindow.getFramebufferWidth() / (float)Minecraft.getInstance().mainWindow.getFramebufferHeight(), 0.05F, (float)Minecraft.getInstance().gameSettings.renderDistanceChunks * 16*2));
		//GlStateManager.enableRescaleNormal();
		//SomeGuns.LOGGER.info(GlStateManager.getMatrix4f(GL11.GL_PROJECTION_MATRIX));

		// 透視投影
		GlStateManager.matrixMode( GL11.GL_MODELVIEW );
		GlStateManager.pushMatrix();
		GlStateManager.loadIdentity();
		GlStateManager.translatef(0,0,-1.0f);
		//SomeGuns.LOGGER.info(GlStateManager.getMatrix4f(GL11.GL_MODELVIEW_MATRIX));

		// attrib設定
		//GlStateManager.translated(width/2,height/2,1);
		GlStateManager.enableBlend();
		GlStateManager.disableAlphaTest();
		GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
		GlStateManager.enableTexture();
		GlStateManager.enableCull();
		//GL11.glEnable(GL11.GL_POLYGON_SMOOTH);

		Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE_REMAIN);


		// transform他
		GlStateManager.rotatef(-10f,0.0f,1.0f,0.0f);
		GlStateManager.rotatef(-1f,0.0f,0.0f,1.0f);
		GlStateManager.rotatef(-1f,1.0f,0.0f,0.0f);
		GlStateManager.scalef(1.0f,1.1f,1.0f);

		GlStateManager.pushMatrix();
		GlStateManager.translatef(0.6f,-0.55f,0f);
		GlStateManager.scalef(0.05f,0.05f,0.05f);

		// 黒背景
		GL11.glColor4f(0.0f, 0.0f, 0.0f, 0.5f);
		GlStateManager.disableTexture();
		GlStateManager.begin(GL11.GL_QUADS);
		GlStateManager.vertex3f(0.8f, 1.2f, -0.1f);
		GlStateManager.vertex3f(0.8f, -1.2f, -0.1f);
		GlStateManager.vertex3f(11.8f, -1.2f, -0.1f);
		GlStateManager.vertex3f(11.8f, 1.2f, -0.1f);
		GlStateManager.end();
		GlStateManager.enableTexture();

		// 残弾数
		textRender(remainText);

		GlStateManager.popMatrix();
		GlStateManager.pushMatrix();
		GlStateManager.translatef(0.92f,-0.53f,0f);
		GlStateManager.scalef(0.03f,0.03f,0.03f);
		textRender("/");
		GlStateManager.translatef(-0.35f,0,0f);
		// 装弾数
		textRender(capacityText);

		GlStateManager.popMatrix();
		//GL11.glDisable(GL11.GL_POLYGON_SMOOTH);
		GlStateManager.disableCull();
		GlStateManager.matrixMode( GL11.GL_PROJECTION );
		GlStateManager.popMatrix();
		GlStateManager.matrixMode( GL11.GL_MODELVIEW );
		GlStateManager.popMatrix();

	}

	public static void reloadRender(float partialTick) {
		//　リロードのくるくる
		int width = Minecraft.getInstance().mainWindow.getScaledWidth();
		int height = Minecraft.getInstance().mainWindow.getScaledHeight();
		GlStateManager.pushMatrix();
		GlStateManager.disableTexture();
		GL11.glColor4f(0.9f, 0.9F, 0.9F, 0.9f);
		GlStateManager.translated(width/2,height/2,0);
		GlStateManager.rotatef(-90f,0.0f,0.0f,1.0f);
		GL11.glEnable(GL11.GL_POLYGON_SMOOTH);


		{
			final float R = 12f;
			final float R2 = 14f;
			final int SPLIT_CIRCLE = 20;
			final float SPLIT_STEP = (float)Math.PI*2/SPLIT_CIRCLE;
			float angle;

			GlStateManager.begin(GL11.GL_QUAD_STRIP);
			for (int i = 0; i <= SPLIT_CIRCLE; i++) {
				angle = i * SPLIT_STEP;
				if((float)i/SPLIT_CIRCLE > (Minecraft.getInstance().world.getGameTime()+partialTick - reloadStartTime)/reloadNeedTime)
					angle = (float)(Math.PI*2*(Minecraft.getInstance().world.getGameTime()+partialTick - reloadStartTime)/reloadNeedTime);
				GlStateManager.vertex3f(R * MathHelper.cos(angle), R * MathHelper.sin(angle), -0.0f);
				GlStateManager.vertex3f(R2 * MathHelper.cos(angle), R2 * MathHelper.sin(angle), -0.0f);
			}
			GlStateManager.end();
		}
		GL11.glDisable(GL11.GL_POLYGON_SMOOTH);
		GlStateManager.enableTexture();
		GlStateManager.popMatrix();

	}

	public static void setReloadStartTime(float need){
		reloadNeedTime = need;
		reloadStartTime = Minecraft.getInstance().world.getGameTime();
	}
	public static void textRender(String str){

		// 表示ポリゴンサイズ
		final float FACTORX = (float)1/4;
		final float FACTORY = (float)1/3;

		for(int i = 0; i < str.length(); i++){
			Vector2f v = texCoord.get(str.charAt(i));
			v = v == null ? new Vector2f(0,0) : v;

			GlStateManager.translatef(2.0f,0,0);

			if(false){
				GL11.glColor4f(0.0f, 0.0f, 0.0f, 0.5f);
				GlStateManager.disableTexture();
				GlStateManager.begin(GL11.GL_QUADS);
				GlStateManager.vertex3f(-1.0f, 1.0f, -0.0f);
				GlStateManager.vertex3f(-1.0f, -1.0f, -0.0f);
				GlStateManager.vertex3f(1.0f, -1.0f, -0.0f);
				GlStateManager.vertex3f(1.0f, 1f, -0.0f);
				GlStateManager.end();
				GlStateManager.enableTexture();
			}

			GL11.glColor4f(0.9f, 0.9F, 0.9F, 0.9f);
			GlStateManager.begin(GL11.GL_QUADS);
			GlStateManager.texCoord2f(v.x,v.y);
			GlStateManager.vertex3f(-1.0f, 1.0f, 0.0f);
			GlStateManager.texCoord2f(v.x,v.y + FACTORY);
			GlStateManager.vertex3f(-1.0f, -1.0f, 0.0f);
			GlStateManager.texCoord2f(v.x+ FACTORX,v.y + FACTORY);
			GlStateManager.vertex3f(1.0f, -1.0f, 0.0f);
			GlStateManager.texCoord2f(v.x+ FACTORX,v.y);
			GlStateManager.vertex3f(1.0f, 1.0f, 0.0f);
			GlStateManager.end();

		}


	}


}
