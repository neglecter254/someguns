package someguns;


import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;

import java.util.function.Supplier;

public class ReloadMessage {  //Imessageは廃止

	public static void encode(ReloadMessage msg, PacketBuffer buf) { }

	public static ReloadMessage decode(PacketBuffer buf) {
		return new ReloadMessage();
	}

	public static void handle(final ReloadMessage message, Supplier<NetworkEvent.Context> ctx) {
		ctx.get().enqueueWork(() -> {
			PlayerEntity player = ctx.get().getSender();
			ItemStack stack = player.getHeldItemMainhand();
			if(stack.getItem() instanceof GunItem) {
				((GunItem)stack.getItem()).reload(stack,player);
			}
		});
		ctx.get().setPacketHandled(true);
	}

}
