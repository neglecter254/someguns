package someguns;

import net.minecraft.entity.*;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.network.PacketBuffer;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.DamageSource;
import net.minecraft.util.IndirectEntityDamageSource;
import net.minecraft.util.math.*;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.common.registry.IEntityAdditionalSpawnData;
import net.minecraftforge.fml.network.FMLPlayMessages;
import net.minecraftforge.fml.network.NetworkHooks;
import net.minecraftforge.fml.network.PacketDistributor;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.util.Comparator.comparing;

public class LaserEntity extends Entity implements IProjectile, IEntityAdditionalSpawnData {
	/*
レーザー版　弾丸
　*重力落下は無し
　*速度はm/sで管理
　*威力減衰もなし
 *Entityを継承

　*弾丸というより弾道オブジェクト
　*生成後移動しない
	*Enitityはなぜか高速移動すると表示がずれるので別openGLで直接描画
    *判定は軌道上を移動させる
    *AbstractArrowEntityを参考に 継承はしない
    *1回/tick当たり判定チェックraytrace
 *消滅
    *ぶつかった後も軌跡をしばらく表示させる
    *描画が終わった後に消滅＝削除
    *ブロックにあたった場合はそこまで
    *ブロック破壊はしない
	*軌道上にmobがいた場合はダメージを与える
		*貫通するかは変数として持つ
        *貫通しない場合は最初に当たるmobのみにダメージを与える
		*貫通する場合は射程まで消滅しない
	*消えるまでの時間を変数で持つ

	motionで終点管理

*/

	private Vec3d startPoint;
	private Vec3d endPoint;
	private Vec3d velocity;
	private double rangeDistance;
	private float damage;
	private int lifetime;
	private boolean penetrate = false;
	private UUID shootingEntity;

	private int age;
	private ArrayList<UUID> PenetratedEntity = new ArrayList<>();

	public LaserEntity(EntityType<? extends Entity> type, World world){
		super(type, world);
	}

	public LaserEntity(World worldIn) {
		this(SomeGuns.LASER, worldIn);
	}


	public LaserEntity(World worldIn, Entity shooter_, Vec3d startPoint_, Vec3d velocity_, double rangeDistance_, float damage_, boolean penetrate_) {
		this(SomeGuns.LASER, worldIn);
		this.startPoint = startPoint_;
		this.velocity = velocity_.scale(0.05d);
		this.rangeDistance = rangeDistance_;
		this.damage = damage_;
		this.shootingEntity = shooter_.getUniqueID();
		this.lifetime = 20*5;
		this.age = 0;
		this.penetrate = penetrate_;
		updateEndPoint();

		setPosition(startPoint.x,startPoint.y,startPoint.z);
		setMotion(velocity_);
		setRotation();
		//LOGGER.info("行きつく先は");
		//LOGGER.info(getEndPoint());
	}

	public LaserEntity(FMLPlayMessages.SpawnEntity spawnEntity, World world) {
		this(SomeGuns.LASER,world);
	}

	public Vec3d getStartPoint() {
		return startPoint;
	}
	public Vec3d getEndPoint(){
		// 毎レンダリング呼ばれるので軽く
		return endPoint;
	}
	public Vec3d getVelocity(){
		return velocity;
	}
	public int getLifetime() {
		return lifetime;
	}

	public int getAge() {
		return age;
	}

	@Override
	protected void readAdditional(CompoundNBT compound) {
		// nbtタグからインスタンスに情報を読み込む大事　savedata -> server
		startPoint = new Vec3d(compound.getDouble("X"),compound.getDouble("Y"),compound.getDouble("Z"));
		velocity = new Vec3d(compound.getDouble("VX"),compound.getDouble("VY"),compound.getDouble("VZ"));
		rangeDistance = compound.getDouble("range");
		damage = compound.getFloat("damage");
		shootingEntity = compound.getUniqueId("shooter");
		lifetime = compound.getInt("lifetime");
		age = compound.getInt("age");
		penetrate = compound.getBoolean("penetrate");
	}

	@Override
	protected void writeAdditional(CompoundNBT compound) {
		// nbtタグへインスタンスから情報を書き込む大事 server -> savedata
		compound.putDouble("X",startPoint.x);
		compound.putDouble("Y",startPoint.y);
		compound.putDouble("Z",startPoint.z);

		compound.putDouble("VX",velocity.x);
		compound.putDouble("VY",velocity.y);
		compound.putDouble("VZ",velocity.z);

		compound.putDouble("range",rangeDistance);
		compound.putFloat("damage",damage);
		compound.putUniqueId("shooter",shootingEntity);
		compound.putInt("lifetime",lifetime);
		compound.putInt("age",age);
		compound.putBoolean("penetrate",penetrate);
	}
	@Override
	public void writeSpawnData(PacketBuffer buffer) {
		// スポーン時にクライアントと同期させたいデータをここに書く
		// クライアント側entity生成は色々省略されている.描画に必要なら必須
		// 描画だけはブロック貫通してもされる
		buffer.writeDouble(startPoint.x);
		buffer.writeDouble(startPoint.y);
		buffer.writeDouble(startPoint.z);
		buffer.writeDouble(velocity.x);
		buffer.writeDouble(velocity.y);
		buffer.writeDouble(velocity.z);
		buffer.writeDouble(rangeDistance);
		buffer.writeInt(lifetime);
	}
	@Override
	public void readSpawnData(PacketBuffer additionalData) {
		// writeSpawnDataから受け取る // serer -> client
		startPoint = new Vec3d(additionalData.readDouble(),additionalData.readDouble(),additionalData.readDouble());
		velocity = new Vec3d(additionalData.readDouble(),additionalData.readDouble(),additionalData.readDouble());
		rangeDistance = additionalData.readDouble();
		lifetime = additionalData.readInt();
		// 終点を計算
		updateEndPoint();
	}
	@Override
	protected void registerData() {
		//　dataManagerでクライアントと常に同期させたいデータを登録する
	}
	@Override
	public IPacket<?> createSpawnPacket() {
		// とりあえず自身を返せばいいらしい
		return NetworkHooks.getEntitySpawningPacket(this);
		//return null;
	}

	public void shoot(double x, double y, double z, float velocity, float inaccuracy) {
		// 多分実装不要
	}
	public void updateEndPoint(){
		// 終点をブロックとの当たり判定までにする
		endPoint = startPoint.add(velocity.normalize().scale(rangeDistance));
		RayTraceResult raytraceresult = this.world.rayTraceBlocks(new RayTraceContext(startPoint, endPoint, RayTraceContext.BlockMode.COLLIDER, RayTraceContext.FluidMode.NONE, this));
		if (raytraceresult.getType() != RayTraceResult.Type.MISS) {
			endPoint = raytraceresult.getHitVec();
		}
	}

	public void setRotation(){
		Vec3d v = getMotion();
		// atan2は+-180
		this.rotationYaw = (float)(MathHelper.atan2(-v.x, v.z) * (double)(180F / (float)Math.PI));
		this.rotationPitch = (float)(MathHelper.atan2(-v.y, MathHelper.sqrt(v.x*v.x+v.z*v.z)) * (double)(180F / (float)Math.PI));
		this.prevRotationYaw = this.rotationYaw;
		this.prevRotationPitch = this.rotationPitch;
	}

	public void tick() {


		// age+1
		++age;

		// start + velocity * age に先頭がいると考える
		// velocity * (age - 1)の間にいるモブにダメージ
		Vec3d prevPos = startPoint.add(velocity.scale(age-1));
		Vec3d nextPos;
		if (velocity.scale(age).lengthSquared() > rangeDistance*rangeDistance) {
			nextPos = startPoint.add(velocity.scale(age));
		}else {
			nextPos = startPoint.add(velocity.normalize().scale(rangeDistance));
		}

		if(!this.world.isRemote) {
			if(collided) {
				// 速度が速いと当たり判定のコストがクソ高
				// 射程外 or 地形と当ったら当たり判定しない

			}else {
				// ブロックとの当たり判定
				RayTraceResult raytraceresult = this.world.rayTraceBlocks(new RayTraceContext(prevPos, nextPos, RayTraceContext.BlockMode.COLLIDER, RayTraceContext.FluidMode.NONE, this));
				if (raytraceresult.getType() != RayTraceResult.Type.MISS) {
					// ブロックに当たったらその場に留まる
					nextPos = raytraceresult.getHitVec();
					collided = true;
				}

				// エンティティとの当たり判定
				List<Entity> entities = world.getEntitiesInAABBexcluding(this, this.getBoundingBox().grow(velocity.length()), (entity) -> {
					// 次のエンティティは除外： 当たり判定のないEntity すでに貫通したEntity ,スペクテイター, 削除済み, 撃ったプレーヤー, 弾丸同士
					return entity.canBeCollidedWith() && !PenetratedEntity.contains(entity.getUniqueID()) && !entity.isSpectator() && entity.isAlive()
							&& entity.getUniqueID() != shootingEntity && !(entity instanceof LaserEntity);
				});
				// 距離でソートしておく
				entities.sort(comparing(x -> x.getDistance(this)));
				for (Entity entity : entities) {
					// エンティティが移動してると当たりにくいのでprevでオフセットしてみる
					Vec3d offprev = new Vec3d(entity.prevPosX - entity.posX,entity.prevPosY - entity.posY,entity.prevPosZ - entity.posZ).scale(0.5f);
					if (AABBCollision(entity.getBoundingBox().offset(offprev), prevPos, nextPos,0.2d)) {
						// 貫通したエンティティを記憶
						PenetratedEntity.add(entity.getUniqueID());

						ServerPlayerEntity shooter = (ServerPlayerEntity) ((ServerWorld) this.world).getEntityByUuid(this.shootingEntity);
						// ダメージ処理
						DamageSource damagesource = new IndirectEntityDamageSource("bullet", this, shooter);
						entity.attackEntityFrom(damagesource, this.damage);
						entity.hurtResistantTime = 0;

						// プレイヤーにヒットを通知
						PacketHandler.INSTANCE.send(PacketDistributor.PLAYER.with(() -> shooter), new HitMessage());
						//LOGGER.info(entity);

						if (!penetrate) {
							this.remove();
							break;
						}
					}
				}
				if(velocity.scale(age).lengthSquared() >= rangeDistance*rangeDistance){
					collided = true;
				}
			}

			// 寿命を迎えたら削除
			if ( age >= lifetime) {
				this.remove();
			}
		}

		// 煙
		//world.addParticle(ParticleTypes.CLOUD,nowPos.x, nowPos.y, nowPos.z, 0.0D, 0.0D, 0.0D);
		// 煙増量
		{
			int n = 0;
			for (int i = 0; i < n; i++)
				world.addParticle(ParticleTypes.CLOUD,
						MathHelper.lerp((i+1d) / (n+1), prevPosX, prevPos.x),
						MathHelper.lerp((i+1d) / (n+1), prevPosY, prevPos.y),
						MathHelper.lerp((i+1d) / (n+1), prevPosZ, prevPos.z),
						0.0D, 0.0D, 0.0D);
		}
		// 移動
		//this.posX = nextPos.x;
		//this.posY = nextPos.y;
		//this.posZ = nextPos.z;
		//this.setPosition(this.posX, this.posY, this.posZ);
	}

	public boolean AABBCollision (AxisAlignedBB aabb, Vec3d start, Vec3d end, double r){
		// 当たり判定は自分で実装 AABBの中意味わからなすぎ
		// AABBはワールド座標
		//　静止したEntity(AABB)と点(線分)で判定

		// 判定が厳しいので少し広げる
		aabb = aabb.grow(r/2.0d);

		if((aabb.minY > start.y && aabb.minY > end.y) || (aabb.maxY < start.y && aabb.maxY < end.y))
			return false;
		if((aabb.minX > start.x && aabb.minX > end.x) || (aabb.maxX < start.x && aabb.maxX < end.x))
			return false;
		if((aabb.minZ > start.z && aabb.minZ > end.z) || (aabb.maxZ < start.z && aabb.maxZ < end.z))
			return false;

		Vec3d vec = end.subtract(start);
		double P1,P2;
		double min,max;
		boolean flagXY = false, flagZY = false, flagXZ = false;
		double aXY = (vec.x/vec.y); // XY平面の傾きの逆数
		double aZY = (vec.z/vec.y); // ZY平面の傾きの逆数
		double aXZ = (vec.x/vec.z); // XZ平面の傾きの逆数

		// XY平面への投影
		P1 = (aabb.maxY-start.y)*aXY;
		P2 = (aabb.minY-start.y)*aXY;
		min = Math.min(P1,P2);
		max = Math.max(P1,P2);
		if(min + start.x <= aabb.maxX && aabb.minX <= max + start.x) flagXY = true;    //　XY平面では接触

		// ZY平面への投影
		P1 = (aabb.maxY-start.y)*aZY;
		P2 = (aabb.minY-start.y)*aZY;
		min = Math.min(P1,P2);
		max = Math.max(P1,P2);
		if(min + start.z <= aabb.maxZ && aabb.minZ <= max + start.z) flagZY = true;    //　ZY平面では接触

		// XZ平面への投影
		P1 = (aabb.maxZ-start.z)*aXZ;
		P2 = (aabb.minZ-start.z)*aXZ;
		min = Math.min(P1,P2);
		max = Math.max(P1,P2);
		if(min + start.x <= aabb.maxX && aabb.minX <= max + start.x) flagXZ = true;    //　XZ平面では接触

		if (flagXY && flagZY && flagXZ){
			// XY平面とZY平面とXZ平面で接触しているなら3次元で接触している
			return true;
		}

		return false;
	}

	@Override
	protected float getEyeHeight(Pose p_213316_1_, EntitySize p_213316_2_) {
		return 0.0F;
	}


}


