package someguns;

public interface ITimerCapability {
	long getTick();
	double getProgress();
	boolean isFinish();
	boolean isRunning();
	void setTick(long tick);
	void stop();
	void addTick();

}
