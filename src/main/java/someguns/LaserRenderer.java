package someguns;

import com.mojang.blaze3d.platform.GlStateManager;
import com.sun.javafx.geom.Matrix3f;
import com.sun.javafx.geom.Vec3f;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Matrix4f;
import net.minecraft.client.renderer.Vector3f;
import net.minecraft.client.renderer.culling.ICamera;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import javax.annotation.Nullable;
import javax.vecmath.Matrix3d;


@OnlyIn(Dist.CLIENT)
public class LaserRenderer extends EntityRenderer<LaserEntity>{
	protected static final Logger LOGGER = LogManager.getLogger();
	// とりえあずTEXTUREは作っておく不要かも
	private static final ResourceLocation TEXTURES = new ResourceLocation("textures/block/gold_block.png");

	protected LaserRenderer(EntityRendererManager renderManager) {
		super(renderManager);
		this.shadowSize = 0.1F;
	}

	@Override
	public boolean shouldRender(LaserEntity livingEntity, ICamera camera, double camX, double camY, double camZ) {
		return true;
	}

	public void doRender(LaserEntity entity, double x, double y, double z, float entityYaw, float partialTicks) {


		GlStateManager.pushMatrix();

		// 絶対座標に変換 x,y,zはカメラからの相対座標
		//GlStateManager.translated(-entity.lastTickPosX+x ,-entity.lastTickPosY+y ,-entity.lastTickPosZ+z);
		GlStateManager.translated(x ,y ,z);
		GlStateManager.rotatef(-entity.rotationYaw, 0.0F, 1.0F, 0.0F);
		GlStateManager.rotatef(entity.rotationPitch, 1.0F, 0.0F, 0.0F);

		// カメラに対して傾かないように面を回転させる
		// aをレーザの向き.bをカメラからaの始点までのベクトルとする
		// bからaへの正射影ベクトル a' = a・b/|a|^2*a
		// 面の向き c = b-a'
		// 回転　a軸中心にΘ = atan2(c)回転させる
		Vec3d a = entity.getVelocity();
		Vec3d b = new Vec3d(x,y,z);
		a = a.scale(a.dotProduct(b)/a.lengthSquared());
		Vec3d c = a.subtract(b).normalize();

		//cはワールド座標なのでモデル座標に変換
		Matrix3d matY = new Matrix3d();
		Matrix3d matX = new Matrix3d();
		Matrix3d mat = new Matrix3d();
		matY.rotY((float)Math.toRadians(-entity.rotationYaw));
		matX.rotX((float)Math.toRadians(entity.rotationPitch));
		mat.setIdentity();
		mat.mul(matY);
		mat.mul(matX);
		mat.invert();
		Vec3d d= new Vec3d(c.x*mat.m00+c.y*mat.m01+c.z*mat.m02,c.x*mat.m10+c.y*mat.m11+c.z*mat.m12,c.x*mat.m20+c.y*mat.m21+c.z*mat.m22);

		GlStateManager.rotatef((float)Math.toDegrees(Math.atan2(d.x,-d.y)),0,0,1);


		//float[] buf = new float[5*4];
		float l = (float)entity.getVelocity().length();
		float[] buf = {
				-0.2f, 0, 0, 0, 0,
				-0.2f, 0, l, 1, 0,
				 0.2f, 0, 0, 0, 1,
				 0.2f, 0, l, 1, 1,
		};

		Render.setInterLeaveArray(buf);
		Render.AdaptVAO();

		// シェーダを使用する
		Render.getShader().useProgram();
		Render.getShader().setPartialTime((entity.getAge()+partialTicks) / entity.getLifetime());

		//GlStateManager.disableCull();
		GL11.glDisable(GL11.GL_CULL_FACE);

		// 描画
		GL11.glDrawArrays(GL11.GL_TRIANGLE_STRIP, 0, 4);

		//LOGGER.info(entity);
		/*
		GlStateManager.begin(GL11.GL_TRIANGLE_STRIP);
		GlStateManager.vertex3f(-0.2f,0,0);
		GlStateManager.vertex3f(-0.2f,0,(float)entity.getVelocity().length());
		GlStateManager.vertex3f(0.2f,0,0);
		GlStateManager.vertex3f(0.2f,0,(float)entity.getVelocity().length());
		GlStateManager.end();
		*/

		/*
		GL11.glLineWidth(100f);
		GlStateManager.begin(GL11.GL_LINES);
		GlStateManager.vertex3f((float)x, (float)y, (float)z);
		GlStateManager.vertex3f((float)x+1 , (float)y+1,(float)z+1);
		//GlStateManager.vertex3f((float)entity.getEndPoint().x , (float)entity.getEndPoint().y,(float)entity.getEndPoint().z);
		GlStateManager.end();

		GlStateManager.begin(GL11.GL_TRIANGLES);
		GlStateManager.vertex3f(0.0f , 0.0f,0.0f);
		GlStateManager.vertex3f( 1.0f, -1.0f,0.0f);
		GlStateManager.vertex3f( 1.0f, 1.0f,0.0f);
		GlStateManager.end();
*/


		// シェーダプログラムをデフォルトに
		GL20.glUseProgram(0);
		Render.DefaultVAO();

		GlStateManager.popMatrix();


	}

	@Nullable
	@Override
	protected ResourceLocation getEntityTexture(LaserEntity entity) {
		return TEXTURES;
	}
}

