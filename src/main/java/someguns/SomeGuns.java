package someguns;

import javafx.scene.input.KeyCode;
import net.minecraft.client.Minecraft;
import net.minecraft.client.network.play.ClientPlayNetHandler;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.event.InputEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.fml.network.FMLPlayMessages;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.glfw.GLFW;

import javax.swing.*;
import java.awt.event.KeyEvent;


// The value here should match an entry in the META-INF/mods.toml file
@Mod(SomeGuns.MODID)
public class SomeGuns
{
    public static final String MODID = "someguns";

    // Directly reference a log4j logger.
    public static final Logger LOGGER = LogManager.getLogger();



    public static EntityType<BulletEntity> BULLET;
    public static EntityType<LaserEntity> LASER;
    public static SoundEvent FIRING;
    public static SoundEvent RELOAD;

    public SomeGuns() {
        // Register the setup method for modloading
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
        // Register the enqueueIMC method for modloading
        //FMLJavaModLoadingContext.get().getModEventBus().addListener(this::enqueueIMC);
        // Register the processIMC method for modloading
        //FMLJavaModLoadingContext.get().getModEventBus().addListener(this::processIMC);
        // Register the doClientStuff method for modloading
        //FMLJavaModLoadingContext.get().getModEventBus().addListener(this::doClientStuff);

        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::initClient);

        // Register ourselves for server and other game events we are interested in
        MinecraftForge.EVENT_BUS.register(this);
    }

    @SubscribeEvent
    public void tickStart(TickEvent.PlayerTickEvent event) {
        //world毎に実行される

        /*
        if (event.phase == TickEvent.Phase.END && event.side == LogicalSide.SERVER) {
            ServerWorld w = event.world.getServer().getWorld(event.world.dimension.getType());
            if(w.getDimension().getType() == DimensionType.OVERWORLD){
                for (PlayerEntity p : w.getPlayers()) {
                    LOGGER.info(p.getCooldownTracker().hasCooldown(p.getHeldItemMainhand().getItem()));
                }
            }
        }*/
        /*
        if (event.phase == TickEvent.Phase.END && event.side == LogicalSide.SERVER) {
            //LOGGER.info(event.world.dimension.getType());
            ServerWorld w = event.world.getServer().getWorld(event.world.dimension.getType());
            for (Long cl : w.getForcedChunks()) {
                Chunk c = w.getChunk(ChunkPos.getX(cl), ChunkPos.getZ(cl));
                boolean flag = w.getPlayers().stream().noneMatch(p -> {
                    double dx = p.posX - c.getPos().x;
                    double dz = p.posZ - c.getPos().z;
                    return !p.isSpectator() && dx * dx + dz * dz < 16384.0D;
                });
                if(flag){
                    w.func_217441_a(c, w.getGameRules().getInt(GameRules.RANDOM_TICK_SPEED));
                    //LOGGER.info(c.getPos().toString());
                }
            }
        }*/
    }
    private void setup(FMLCommonSetupEvent event) {
        // パケットハンドラの登録
        PacketHandler.register();
        // キャパビリティの登録
        CapabilityManager.INSTANCE.register(ITimerCapability.class, new TimerCapabilityStorage(), TimerCapablity::new);
    }

    @SubscribeEvent
    public void onAttachCapabilities(AttachCapabilitiesEvent<Entity> event) {
        if (event.getObject() instanceof PlayerEntity) {
            event.addCapability(new ResourceLocation(MODID, "timer"), new TimerCapabilityProvider());
        }
    }

    private void initClient(final FMLClientSetupEvent event) {
        // Entityのレンダー登録
        RenderingRegistry.registerEntityRenderingHandler(LaserEntity.class, LaserRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(BulletEntity.class, BulletRenderer::new);
        //static Method x = ObfuscationReflectionHelper.findMethod(GameRenderer.class,"func_78482_e",float.class);
        //x.setAccessible(true);
        //Minecraft.getInstance().renderViewEntity
        OverlayRender.initTexCoord();


    }

    @SubscribeEvent
    public void playerTick(TickEvent.PlayerTickEvent event) {
        if (event.side.isClient() && event.phase == TickEvent.Phase.END) {
            if(event.player != Minecraft.getInstance().player)
                return ;         //別playerでも表示されてしまう対策
            if (event.player.getHeldItemMainhand().getItem() instanceof GunItem) {
                OverlayRender.setVisibleText(true);
                GunItem.setGUIText(event.player.getHeldItemMainhand());
            } else {
                OverlayRender.setVisibleText(false);
            }

            ReloadManager.tick(event);
        }
    }


    @OnlyIn(Dist.CLIENT)
    @SubscribeEvent
    public void keyInput(InputEvent.KeyInputEvent event) {

        if (event.getAction() == GLFW.GLFW_PRESS) { // press
            if( event.getKey() == GLFW.GLFW_KEY_R) {
                // リロード開始
                ReloadManager.startReload();
            }
            if( event.getKey() == GLFW.GLFW_KEY_UP) {
                //OverlayRender.z += 1;
            }else if(event.getKey() == GLFW.GLFW_KEY_DOWN){
                //OverlayRender.z -= 1;
            }

        } else if (event.getAction() == GLFW.GLFW_RELEASE) { // release
            //　何もしない
        }

    }

    @OnlyIn(Dist.CLIENT)
    @SubscribeEvent
    public void RightClickInput(InputEvent.MouseInputEvent event) {
        if (event.getAction() == GLFW.GLFW_PRESS) {
            if( event.getButton() == GLFW.GLFW_MOUSE_BUTTON_RIGHT) {
                MouseClickManager.setRight(true);
            }else if( event.getButton() == GLFW.GLFW_MOUSE_BUTTON_LEFT) {
                MouseClickManager.setLeft(true);
            }
        } else if (event.getAction() == GLFW.GLFW_RELEASE) {
            if( event.getButton() == GLFW.GLFW_MOUSE_BUTTON_RIGHT) {
                MouseClickManager.setRight(false);
            }else if( event.getButton() == GLFW.GLFW_MOUSE_BUTTON_LEFT) {
                MouseClickManager.setLeft(false);
            }
        }
    }

    @OnlyIn(Dist.CLIENT)
    @SubscribeEvent
    public void OverlayRender(RenderGameOverlayEvent.Pre event) {
        // シェーダの初期化
        Render.getRender();
        Render.getShader();

        if(event.getType() == RenderGameOverlayEvent.ElementType.ALL) {
            OverlayRender.hitmarkRender(event.getPartialTicks());
            OverlayRender.HUDRender(event.getPartialTicks());
            if(ReloadManager.isReloading())
                OverlayRender.reloadRender(event.getPartialTicks());
        }
    }



    // You can use EventBusSubscriber to automatically subscribe events on the contained class (this is subscribing to the MOD
    // Event bus for receiving Registry Events)
    @Mod.EventBusSubscriber(bus=Mod.EventBusSubscriber.Bus.MOD)
    public static class RegistryEvents {
        /*@SubscribeEvent
        public static void onBlocksRegistry(final RegistryEvent.Register<Block> blockRegistryEvent) {
            // register a new block here
            LOGGER.info("HELLO from Register Block");
        }*/
        @SubscribeEvent
        public static void onItemRegistry(final RegistryEvent.Register<Item> event) {
            // register a new block here
            LOGGER.info("HELLO from Register Item");
            // タブ分類
            Item.Properties builder = new Item.Properties().group(ItemGroup.COMBAT);

            //Item作成
            Item ak47 =  new AK47Item(builder);
            ak47.setRegistryName(new ResourceLocation(MODID, "ak47"));

            Item railgun =  new RailgunItem(builder);
            railgun.setRegistryName(new ResourceLocation(MODID, "railgun"));

            Item _762bullet =  new _762BulletItem(builder);
            _762bullet .setRegistryName(new ResourceLocation(MODID, "_762bullet"));

            Item magazine =  new MagazineItem(builder);
            magazine.setRegistryName(new ResourceLocation(MODID, "magazine"));

            Item _762magazine =  new _762MagazineItem(builder);
            _762magazine.setRegistryName(new ResourceLocation(MODID, "_762magazine"));

            // 登録
            event.getRegistry().registerAll(ak47,railgun,_762bullet,magazine,_762magazine);

        }
        @SubscribeEvent
        public static void registerEntityTypes(final RegistryEvent.Register<EntityType<?>> event) {

            BULLET = EntityType.Builder.<BulletEntity>create(BulletEntity::new, EntityClassification.MISC)
                    .setCustomClientFactory((FMLPlayMessages.SpawnEntity spawnEntity, World world) -> new BulletEntity(spawnEntity,world))
                    .setTrackingRange(60)
                    .setUpdateInterval(1)
                    .setShouldReceiveVelocityUpdates(true)
                    .size(0.5f, 0.5f)
                    .build(MODID + ":bullet");;
            BULLET.setRegistryName(new ResourceLocation(MODID, "bullet"));
            //event.getRegistry().register(BULLET);

            LASER = EntityType.Builder.<LaserEntity>create(LaserEntity::new, EntityClassification.MISC)
                    .setCustomClientFactory((FMLPlayMessages.SpawnEntity spawnEntity, World world) -> new LaserEntity(spawnEntity,world))
                    .setTrackingRange(60)
                    .setUpdateInterval(1)
                    .setShouldReceiveVelocityUpdates(true)
                    .size(0.5f, 0.5f)
                    .build(MODID + ":laser");;
            LASER.setRegistryName(new ResourceLocation(MODID, "laser"));
            //event.getRegistry().register(LASER);
            event.getRegistry().registerAll(BULLET,LASER);
        }

        @SubscribeEvent
        public static void registerSoundEvents(final RegistryEvent.Register<SoundEvent> event) {
            FIRING = makeSoundEvent("firinglight");
            RELOAD = makeSoundEvent("reload");
            event.getRegistry().registerAll(FIRING,RELOAD);
        }



    }

/*
    @SubscribeEvent
    public void onRenderTick(TickEvent.RenderTickEvent event){
        if(event.phase == TickEvent.Phase.START ){
            LOGGER.info("Render resistry3");
            GL11.glTranslatef(100,100,100);
            GL13.glRotated(90,0,1,0);
            //GlStateManager.rotatef(entityYaw+90, 0.0F, 1.0F, 0.0F);
            GlStateManager.rotatef(90, 0.0F, 0.0F, 1.0F);
        }
    }
    @SubscribeEvent
    public void rendering(RenderWorldLastEvent event){
        LOGGER.info("Render resistry3");
        GL11.glTranslatef(100,100,100);
        GL13.glRotated(90,0,1,0);
        //GlStateManager.rotatef(entityYaw+90, 0.0F, 1.0F, 0.0F);
        GlStateManager.rotatef(90, 0.0F, 0.0F, 1.0F);
    }
*/
    private static SoundEvent makeSoundEvent(String name) {
        ResourceLocation shotSoundLocation = new ResourceLocation(MODID, name);
        SoundEvent soundEvent = new SoundEvent(shotSoundLocation);
        soundEvent.setRegistryName(shotSoundLocation);
        return soundEvent;
    }
}
