package someguns;

import net.minecraft.block.BlockState;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.entity.*;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.entity.projectile.ProjectileHelper;
import net.minecraft.item.Item;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.DamageSource;
import net.minecraft.util.IndirectEntityDamageSource;
import net.minecraft.util.math.*;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.network.FMLPlayMessages;
import net.minecraftforge.fml.network.NetworkHooks;
import net.minecraftforge.fml.network.PacketDistributor;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.util.Comparator.comparing;

public class BulletEntity extends Entity implements IProjectile {
	private Vec3d startPoint;
	private Vec3d velocity;
	private double rangeDistance;
	private float damage;
	private boolean penetrate = false;
	private UUID shootingEntity;
	private ArrayList<UUID> PenetratedEntity = new ArrayList<>();

	public BulletEntity(EntityType<? extends Entity> type, World world){
		super(type, world);
	}

	public BulletEntity(World worldIn) {
		this(SomeGuns.BULLET, worldIn);
	}

	public BulletEntity(World worldIn,Entity shooter_,Vec3d startPoint_,Vec3d velocity_,double rangeDistance_, float damage_,boolean penetrate_) {
		this(SomeGuns.BULLET, worldIn);
		this.startPoint = startPoint_;
		this.velocity = velocity_.scale(0.05d);
		this.rangeDistance = rangeDistance_;
		this.damage = damage_;
		this.penetrate = penetrate_;
		this.shootingEntity = shooter_.getUniqueID();
		setPosition(startPoint.x,startPoint.y,startPoint.z);
		setMotion(velocity);
		setRotation();
	}

	public BulletEntity(FMLPlayMessages.SpawnEntity spawnEntity, World world) {
		this(SomeGuns.BULLET,world);
	}

	@Override
	protected void registerData() {
		//　dataManagerでクライアントと常に同期させたいパラメータを登録する
		//　特に実装しなくていい
		// 撃ったプレイヤーは必要かも
	}

	@Override
	protected void readAdditional(CompoundNBT compound) {
		// nbtタグからインスタンスに情報を読み込む
		//this.startPoint = compound.getShort("life");

		startPoint = new Vec3d(compound.getDouble("X"),compound.getDouble("Y"),compound.getDouble("Z"));
		velocity = new Vec3d(compound.getDouble("VX"),compound.getDouble("VY"),compound.getDouble("VZ"));
		rangeDistance = compound.getDouble("range");
		damage = compound.getFloat("damage");
		shootingEntity = compound.getUniqueId("shooter");
	}

	@Override
	protected void writeAdditional(CompoundNBT compound) {
		// nbtタグへインスタンスから情報を書き込む
		compound.putDouble("X",startPoint.x);
		compound.putDouble("Y",startPoint.y);
		compound.putDouble("Z",startPoint.z);

		compound.putDouble("VX",velocity.x);
		compound.putDouble("VY",velocity.y);
		compound.putDouble("VZ",velocity.z);

		compound.putDouble("range",rangeDistance);
		compound.putFloat("damage",damage);
		compound.putUniqueId("shooter",shootingEntity);

	}

	@Override
	public IPacket<?> createSpawnPacket() {
		// とりあえず自身を返せばいいらしい
		return NetworkHooks.getEntitySpawningPacket(this);
		//return null;
	}

	public void shoot(double x, double y, double z, float velocity, float inaccuracy) {
		/*
		Vec3d vec3d = (new Vec3d(x, y, z)).normalize().add(this.rand.nextGaussian() * (double)0.0075F * (double)inaccuracy, this.rand.nextGaussian() * (double)0.0075F * (double)inaccuracy, this.rand.nextGaussian() * (double)0.0075F * (double)inaccuracy).scale((double)velocity);
		this.setMotion(vec3d);
		float f = MathHelper.sqrt(func_213296_b(vec3d));
		this.rotationYaw = (float)(MathHelper.atan2(vec3d.x, vec3d.z) * (double)(180F / (float)Math.PI));
		this.rotationPitch = (float)(MathHelper.atan2(vec3d.y, (double)f) * (double)(180F / (float)Math.PI));
		this.prevRotationYaw = this.rotationYaw;
		this.prevRotationPitch = this.rotationPitch;*/
	}
	public void setRotation(){
		Vec3d v = getMotion();
		// atan2は+-180
		this.rotationYaw = (float)(MathHelper.atan2(-v.x, v.z) * (double)(180F / (float)Math.PI));
		this.rotationPitch = (float)(MathHelper.atan2(-v.y, MathHelper.sqrt(v.x*v.x+v.z*v.z)) * (double)(180F / (float)Math.PI));
		this.prevRotationYaw = this.rotationYaw;
		this.prevRotationPitch = this.rotationPitch;
	}

	public void tick() {

		// 初期化
		this.prevPosX = this.posX;
		this.prevPosY = this.posY;
		this.prevPosZ = this.posZ;

		// 仮移動
		Vec3d nowPos = new Vec3d(this.posX, this.posY, this.posZ);
		Vec3d nextPos = nowPos.add(getMotion());

		//setRotation();

		if(!this.world.isRemote) {
			// ブロックとの当たり判定
			RayTraceResult raytraceresult = this.world.rayTraceBlocks(new RayTraceContext(nowPos, nextPos, RayTraceContext.BlockMode.COLLIDER, RayTraceContext.FluidMode.NONE, this));
			if (raytraceresult.getType() != RayTraceResult.Type.MISS) {
				nextPos = raytraceresult.getHitVec();
				this.remove();
			}

			// エンティティとの当たり判定
			List<Entity> entities = world.getEntitiesInAABBexcluding(this, this.getBoundingBox().grow(velocity.length()), (entity) -> {
						// 次のエンティティは除外： 当たり判定のないEntity すでに貫通したEntity ,スペクテイター, 削除済み, 撃ったプレーヤー, 弾丸同士
						return entity.canBeCollidedWith() && !PenetratedEntity.contains(entity.getUniqueID()) && !entity.isSpectator() && entity.isAlive()
								&& entity.getUniqueID() != shootingEntity && !(entity instanceof BulletEntity) ;});
			// 距離でソートしておく
			entities.sort(comparing(x -> x.getDistance(this)));
			for (Entity entity : entities ) {
				Vec3d offprev = new Vec3d(entity.prevPosX - entity.posX,entity.prevPosY - entity.posY,entity.prevPosZ - entity.posZ).scale(0.5f);
				if (AABBCollision(entity.getBoundingBox().offset(offprev), nowPos, nextPos,0.1d)) {
					// 貫通したエンティティを記憶
					PenetratedEntity.add(entity.getUniqueID());

					ServerPlayerEntity shooter = (ServerPlayerEntity) ((ServerWorld)this.world).getEntityByUuid(this.shootingEntity);
					// ダメージ処理
					DamageSource damagesource = new IndirectEntityDamageSource("bullet",this,shooter);
					entity.attackEntityFrom(damagesource, this.damage);
					entity.hurtResistantTime = 0;

					// プレイヤーにヒットを通知
					PacketHandler.INSTANCE.send(PacketDistributor.PLAYER.with( () -> shooter),new HitMessage());
					//LOGGER.info(entity);

					if(!penetrate) {
						this.remove();
						break;
					}
				}
			}

			// 射程距離を超えたら削除
			if (startPoint.squareDistanceTo(posX, posY, posZ) > rangeDistance * rangeDistance) {
				this.remove();

			}
		}

		// 煙
		world.addParticle(ParticleTypes.CLOUD,nowPos.x, nowPos.y, nowPos.z, 0.0D, 0.0D, 0.0D);
		// 煙増量
		{
			int n = 0;
			for (int i = 0; i < n; i++)
				world.addParticle(ParticleTypes.CLOUD,
						MathHelper.lerp((i+1d) / (n+1), prevPosX, nowPos.x),
						MathHelper.lerp((i+1d) / (n+1), prevPosY, nowPos.y),
						MathHelper.lerp((i+1d) / (n+1), prevPosZ, nowPos.z),
						0.0D, 0.0D, 0.0D);
		}
		// 移動
		this.posX = nextPos.x;
		this.posY = nextPos.y;
		this.posZ = nextPos.z;
		this.setPosition(this.posX, this.posY, this.posZ);
	}

	public boolean AABBCollision (AxisAlignedBB aabb, Vec3d start, Vec3d end, double r){
		// 当たり判定は自分で実装 AABBの中意味わからなすぎ
		// AABBはワールド座標
		//　静止したEntity(AABB)と点(線分)で判定

		// 判定が厳しいので少し広げる
		aabb = aabb.grow(r/2.0d);

		if((aabb.minY > start.y && aabb.minY > end.y) || (aabb.maxY < start.y && aabb.maxY < end.y))
			return false;
		if((aabb.minX > start.x && aabb.minX > end.x) || (aabb.maxX < start.x && aabb.maxX < end.x))
			return false;
		if((aabb.minZ > start.z && aabb.minZ > end.z) || (aabb.maxZ < start.z && aabb.maxZ < end.z))
			return false;

		Vec3d vec = end.subtract(start);
		double P1,P2;
		double min,max;
		boolean flagXY = false, flagZY = false, flagXZ = false;
		double aXY = (vec.x/vec.y); // XY平面の傾きの逆数
		double aZY = (vec.z/vec.y); // ZY平面の傾きの逆数
		double aXZ = (vec.x/vec.z); // XZ平面の傾きの逆数

		// XY平面への投影
		P1 = (aabb.maxY-start.y)*aXY;
		P2 = (aabb.minY-start.y)*aXY;
		min = Math.min(P1,P2);
		max = Math.max(P1,P2);
		if(min + start.x <= aabb.maxX && aabb.minX <= max + start.x) flagXY = true;    //　XY平面では接触

		// ZY平面への投影
		P1 = (aabb.maxY-start.y)*aZY;
		P2 = (aabb.minY-start.y)*aZY;
		min = Math.min(P1,P2);
		max = Math.max(P1,P2);
		if(min + start.z <= aabb.maxZ && aabb.minZ <= max + start.z) flagZY = true;    //　ZY平面では接触

		// XZ平面への投影
		P1 = (aabb.maxZ-start.z)*aXZ;
		P2 = (aabb.minZ-start.z)*aXZ;
		min = Math.min(P1,P2);
		max = Math.max(P1,P2);
		if(min + start.x <= aabb.maxX && aabb.minX <= max + start.x) flagXZ = true;    //　XZ平面では接触

		if (flagXY && flagZY && flagXZ){
			// XY平面とZY平面とXZ平面で接触しているなら3次元で接触している
			return true;
		}

		return false;
	}

	@Override
	protected float getEyeHeight(Pose p_213316_1_, EntitySize p_213316_2_) {
		return 0.0F;
	}

}

/*
弾丸
　*重力落下は無し
　*速度はm/sで管理
　*威力減衰もなし

 *Entityを継承

　*弾丸というより弾道オブジェクト？
　*単位時間当たりに一定量移動する
	*単位時間 = 1tick
    *移動方法はワープ
    *通った軌跡にはパーティクル的なのを残す
    *AbstractArrowEntityを参考に 継承はしない
    *1回/tick当たり判定チェックraytrace

 *ブロックにあたった場合は即消滅
    *液体も即消滅
 *ワープの軌道上にmobがいた場合はダメージを与える
    *貫通するかは変数として持つ
   　*貫通しない場合は最初に当たるmobのみにダメージを与えて消滅
    *貫通する場合は射程まで消滅しない

*/

